package infrastruktur;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.GregorianCalendar;

/**
 * Die Klasse implementiert das Erstellen und Konvertieren von gultigen Datum.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class DateBuilder {

    /**
     * Methode generiert ein gueltiges aktuelles Datum und liefert dieses in Form eines GregorianCalendar zurück.
     *
     * @return Liefert das aktuelle Datum vom Typ GregorianCalendar zurück.
     */
    public static GregorianCalendar getAktuellesDatum() {
        GregorianCalendar deadlineDate = new GregorianCalendar();
        deadlineDate.getTime();
        return deadlineDate;
    }

    /**
     * Die Methode convertiert den Typ LocalDate zu einem GregorianCalender Objekt und gibt diesen zurück.
     *
     * @param datum übergebenes Datum vom Typ LocalDate
     * @return gibt das formatierte Datum vom Typ GregorianCalendar zurück.
     */
    public static GregorianCalendar convertLocalDateTpGregorianCalendar(LocalDate datum) {
        GregorianCalendar convertedDate = GregorianCalendar.from(datum.atStartOfDay(ZoneId.systemDefault()));
        return convertedDate;
    }

}
