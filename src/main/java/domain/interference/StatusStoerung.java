package domain.interference;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Die ENUM-Klasse implementiert die verschiedenen Arten von Status inkl. Beschreibung und eine Liste davon.
 * Die Angelegten ENUMs können beliebig erweitert werden, nur sollte die Anzahl an Zeichen, welche über eine
 * statische Variable(ANZAHL_ZEICHEN_STATUS) definiert wird, eingehalten, oder der Variablenwert angepasst werden.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public enum StatusStoerung implements Serializable {
    //ENUMs
    OFFEN("Offen"),
    ERLEDIGT("Erledigt"),
    IN_ARBEIT("in Arbeit");

    //Datenfelder
    public final String LABEL; //Beschreibung des Status
    private static List<StatusStoerung> STATUS_STOERUNG_LIST = new ArrayList<>();

    /**
     * Konstruktor setzt die Beschreibung der StatusStoerung.
     *
     * @param LABEL übergebene Beschreibung vom Typ String.
     */
    StatusStoerung(String LABEL) {
        this.LABEL = LABEL;
    }

    /**
     * Die Methode liefert die Beschreibung der StatusStoerung(ENUM) als String zurück.
     *
     * @return Liefert die Beschreibung der StatusStoerung vom Typ String zurück.
     */
    public String getStatusStoerungBeschreibung() {
        return this.LABEL;
    }

    /**
     * Die Methode füllt eine Liste mit eindeutigen Einträgen an StatusStörung.
     */
    static {
        if (!STATUS_STOERUNG_LIST.contains(StatusStoerung.values())) {
            for (StatusStoerung statusStoerung : StatusStoerung.values()) {
                STATUS_STOERUNG_LIST.add(statusStoerung);
            }
        }
    }

    /**
     * Die statische Methode liefert eine Kopie einer Liste von StatusStörung zurück.
     *
     * @return Liefert eine gibt eine Kopie einer Liste vom Typ StatusStörung zurück.
     */
    public static List<StatusStoerung> getStatusStoerungenList() {
        return List.copyOf(STATUS_STOERUNG_LIST);
    }

    /**
     * Die statische Methode gibt eine formatierte Ausgabe einer Liste von StatusStörung aus.
     */
    public static void statusStoerungenListAusgeben() {
        int index = 1;
        for (StatusStoerung statusStoerung : STATUS_STOERUNG_LIST) {
            System.out.println(index + " " + statusStoerung.LABEL);
            index++;
        }
    }

}
