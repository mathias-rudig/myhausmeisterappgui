package ui;

import com.hausmeister.sample.HausmeisterApp;
import domain.buildingstructure.RaumListe;
import domain.buildingstructure.StockwerkListe;
import domain.interference.SammlungStoerungen;

/**
 * Die Klasse stellt gewisse Datenfelder und Methoden bereit, von der unsere restlichen Controller erben,
 * um Codeduplikationen zu vermeiden.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class BaseControllerState {
    private SammlungStoerungen sammlungStoerungen;
    private RaumListe raumliste;
    private StockwerkListe stockwerkListe;

    /**
     * Der Konstruktor holt sich die Daten aus dem GlobalContext(HashMap) und weist sie dem jeweiligen Typ zu.
     */
    public BaseControllerState() {
        sammlungStoerungen = (SammlungStoerungen) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_SAMMLUNG_STOERUNGEN);
        raumliste = (RaumListe) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_RAUM_LISTE);
        stockwerkListe = (StockwerkListe) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_STOCKWERK_LISTE);
    }

    public SammlungStoerungen getSammlungStoerungen() {
        return sammlungStoerungen;
    }

    public RaumListe getRaumliste() {
        return raumliste;
    }

    public StockwerkListe getStockwerkListe(){
        return stockwerkListe;
    }

}
