package ui;

import com.hausmeister.sample.HausmeisterApp;
import domain.buildingstructure.Raum;
import domain.interference.Gewerke;
import domain.interference.StatusStoerung;
import infrastruktur.DateBuilder;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

import java.time.LocalDate;
import java.util.GregorianCalendar;
import java.util.Optional;

public class InputStoerungController extends BaseControllerState {
    /**
     * Die Methode fordert den Benutzer auf ein Titel einzugeben und liefert die Eingabe als String zurück.
     *
     * @return Liefert einen Titel vom Typ String zurück.
     */
    public String eingabeStoerungTitel() {
        TextInputDialog dialog = new TextInputDialog("Titel");
        dialog.setTitle("Eingabe Titel");
        dialog.setHeaderText("Bitte geben Sie einene Titel ein.");
        dialog.setContentText("Titel: ");
        Optional<String> result = dialog.showAndWait(); //liefert ein Optinal zurück
        if (result.isPresent()) { //wenn etwas zurück geliefert wurde
            try {
                return result.get();
            } catch (NumberFormatException numberFormatException) { //unchecked Exception
                HausmeisterApp.zeigeFehlerDialog("Kein Titel eingegeben.");
            }
        }
        return null;
    }

    /**
     * Die Methode fordert den Benutzer auf eine Beschreibung einzugeben und liefert die Eingabe als String zurück.
     *
     * @return Liefert eine Beschreibung vom Typ String zurück.
     */
    public String eingabeStoerungBeschreibung() {
        TextInputDialog dialog = new TextInputDialog("Beschreibung");
        dialog.setTitle("Eingabe Beschreibung");
        dialog.setHeaderText("Bitte geben Sie einene Beschreibung ein.");
        dialog.setContentText("Beschreibung: ");
        Optional<String> result = dialog.showAndWait(); //liefert ein Optinal zurück
        if (result.isPresent()) { //wenn etwas zurück geliefert wurde
            try {
                return result.get();
            } catch (NumberFormatException numberFormatException) { //unchecked Exception
                HausmeisterApp.zeigeFehlerDialog("Kein Beschreibung eingegeben.");
            }
        }
        return null;
    }

    /**
     * Die Methode fordert den Benutzer auf ein Gewerk aus der GewerkeListe auszuwählen und liefert die Eingabe als
     * Gewerke zurück.
     *
     * @return Liefert ein Gewerk vom Typ Gewerke zurück.
     */
    public Gewerke eingabeStoerungGewerk() {
        ChoiceDialog<Gewerke> dialog = new ChoiceDialog(Gewerke.ALLGEMEIN, Gewerke.getGewerkeList());
        dialog.setTitle("Gwerke wählen");
        dialog.setHeaderText("Bitte wählen Sie ein Gewerk aus.");
        dialog.setContentText("Gewerk: ");
        Optional<Gewerke> gewerke = dialog.showAndWait();
        if (gewerke.isPresent()) { //wenn etwas zurück geliefert wurde
            try {
                return gewerke.get();
            } catch (NumberFormatException numberFormatException) { //unchecked Exception
                HausmeisterApp.zeigeFehlerDialog("Kein Gewerk gewählt.");
            }
        }
        return null;
    }

    /**
     * Die Methode fordert den Benutzer auf einen Raum aus der Raumliste auzuwählen und liefert die Eingabe als
     * Raum zurück.
     *
     * @return Liefert einen Raum vom Typ Raum zurück.
     */
    public Raum eingabeStoerungRaum() {
        ChoiceDialog<Raum> dialog = new ChoiceDialog(getRaumliste().getRaumListe().get(0), getRaumliste().getRaumListe());
        dialog.setTitle("Raum wählen");
        dialog.setHeaderText("Bitte wählen Sie einn Raum aus.");
        dialog.setContentText("Raum: ");
        Optional<Raum> raum = dialog.showAndWait();
        if (raum.isPresent()) { //wenn etwas zurück geliefert wurde
            try {
                return raum.get();
            } catch (NumberFormatException numberFormatException) { //unchecked Exception
                HausmeisterApp.zeigeFehlerDialog("Kein Raum gewählt.");
            }
        }
        return null;

    }

    /**
     * Die Methode fordert den Benutzer auf einen Status aus der StoerungStatusListe einzugeben und liefert
     * die Eingabe als StatusStoerung zurück.
     *
     * @return Liefert einen  Status vom Typ StatusStoerung zurück.
     */
    public StatusStoerung eingabeStoerungStatus() {
        ChoiceDialog<StatusStoerung> dialog = new ChoiceDialog(StatusStoerung.OFFEN, StatusStoerung.getStatusStoerungenList());
        dialog.setTitle("Status wählen");
        dialog.setHeaderText("Bitte wählen Sie einen Status aus.");
        dialog.setContentText("Status: ");
        Optional<StatusStoerung> statusStoerung = dialog.showAndWait();
        if (statusStoerung.isPresent()) { //wenn etwas zurück geliefert wurde
            try {
                return statusStoerung.get();
            } catch (NumberFormatException numberFormatException) { //unchecked Exception
                HausmeisterApp.zeigeFehlerDialog("Kein Status gewählt.");
            }
        }
        return null;
    }

    /**
     * Die Methode fordert den Benutzer auf ein Datum der Deadline einzugeben und liefert die Eingabe als
     * GregorianCalendar zurück.
     *
     * @return Liefert ein Datum in der Zukunft vom Typ GregorianCalendar zurück.
     */
    public GregorianCalendar eingabeStoerungDeadlineDatum() {
        popUpDatePicker();
        return (GregorianCalendar) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_GEWAEHLTE_DEADLINE);
    }

    /**
     * Die Methode erstellt und öffnet ein Dialogfenster, und bietet einen DatumPicker-welcher den eingegebnen Wert
     * in den Global Context.
     * (Diese Methode wurde aus dem Internet kopiert und nur abgeändert.
     * https://stackoverflow.com/questions/44147595/get-more-than-two-inputs-in-a-javafx-dialog)
     */
    public void popUpDatePicker() {
        Dialog<Results> dialog = new Dialog<>();
        dialog.setTitle("DATUM DEADLINE");
        dialog.setHeaderText("WÄHLEN SIE EIN DATUM");
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        DatePicker datePicker = new DatePicker(LocalDate.now());
        dialogPane.setContent(new VBox(8, datePicker));
        dialog.setResultConverter((ButtonType button) -> {
            if (button == ButtonType.OK) {
                return new Results(datePicker.getValue());
            }
            return null;
        });
        Optional<Results> optionalResult = dialog.showAndWait();
        optionalResult.ifPresent((Results results) -> {
            LocalDate myDate = results.date;
            GlobalContext.getGlobalContext().putStateFor(HausmeisterApp.GLOBAL_GEWAEHLTE_DEADLINE, DateBuilder.convertLocalDateTpGregorianCalendar(myDate));
        });
    }

    /**
     * Statische Klasse zum Speichern des Resultates vom DatePicker.
     */
    private static class Results {

        LocalDate date;

        public Results(LocalDate date) {
            this.date = date;
        }
    }
}
