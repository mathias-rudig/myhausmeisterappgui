package ui;

import com.hausmeister.sample.HausmeisterApp;
import domain.buildingstructure.Raum;
import domain.buildingstructure.Stockwerk;
import domain.interference.Stoerung;
import domain.interference.StoerungMitDeadline;
import exceptions.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.GregorianCalendar;
import java.util.Optional;

public class MainController extends InputStoerungController {

    //FXML-IDs  //Abschnitt S
    @FXML
    private Button btnClose;

    @FXML
    private TableView<Stoerung> tableView;

    /**
     * Die Methode wird zum Initialisieren der Grafischen Oberfläche verwendet.
     */
    public void initialize() {
        this.populateTable();
        this.refreshAllGuiValues();
        this.closeButton();
    }

    /**
     * Setzt die Funktion die EXIT-Buttons der GUI, damit er die Applikation beenden kann.
     */
    private void closeButton() {
        btnClose.setOnAction((ActionEvent event) -> {
            Platform.exit(); //beendet die Applikation
        });
    }

    /**
     * Die Methode implementiert das Aktualisieren der Datenfelder, welche angezeigt werden.
     */
    //Abschnitt Refactoring
    private void refreshAllGuiValues() {
        tableView.getItems().setAll(getSammlungStoerungen().getSammlungStoerung());
    }

    /**
     * DIe Methode implementiert das Erzeugen einer Tabelle mit definierten Spalten und fügt Datensätze aus der
     * Wallet hinzu.
     */
    //Abschnitt Refactoring
    private void populateTable() {
        //Spalten der Tabelle definieren über Getter-Methoden
        TableColumn<Stoerung, String> titel = new TableColumn<>("TITEL");
        titel.setCellValueFactory(new PropertyValueFactory<>("titel"));

        TableColumn<Stoerung, String> status = new TableColumn<>("STATUS");
        status.setCellValueFactory(new PropertyValueFactory<>("statusLabel"));

        TableColumn<Stoerung, String> gewerk = new TableColumn<>("GEWERK");
        gewerk.setCellValueFactory(new PropertyValueFactory<>("gewerkLabel"));

        TableColumn<Stoerung, String> beschreibung = new TableColumn<>("BESCHREIBUNG");
        beschreibung.setCellValueFactory(new PropertyValueFactory<>("beschreibung"));

        TableColumn<Stoerung, String> raumbezeichnung = new TableColumn<>("RAUM");
        raumbezeichnung.setCellValueFactory(new PropertyValueFactory<>("raum"));

        TableColumn<Stoerung, String> stockwerk = new TableColumn<>("STOCKWERK");
        stockwerk.setCellValueFactory(new PropertyValueFactory<>("stockwerk"));

        TableColumn<Stoerung, GregorianCalendar> erstelldatum = new TableColumn<>("ERSTELLDATUM");
        erstelldatum.setCellValueFactory(new PropertyValueFactory<>("erstellungsdatumToString"));

        TableColumn<Stoerung, GregorianCalendar> deadline = new TableColumn<>("DEADLINE");
        deadline.setCellValueFactory(new PropertyValueFactory<>("deadlineDatumToString"));

        //Spalten hinzufügen
        tableView.getColumns().clear(); //zu erst alles löschen
        tableView.getColumns().add(titel);
        tableView.getColumns().add(status);
        tableView.getColumns().add(gewerk);
        tableView.getColumns().add(beschreibung);
        tableView.getColumns().add(raumbezeichnung);
        tableView.getColumns().add(stockwerk);
        tableView.getColumns().add(erstelldatum);
        tableView.getColumns().add(deadline);

    }

    /**
     * Setzt die Funktion des Buttons, der das Änderungsmenue öffnen soll.
     */
    public void oeffneAenderungMenue() {
        Stoerung stoerung = this.tableView.getSelectionModel().getSelectedItem(); //zuweisen des ausgewählten Objekts
        if (stoerung == null) {
            HausmeisterApp.zeigeFehlerDialog("Bitte wählen Sie eine Stoerung aus.");
        } else {
            GlobalContext.getGlobalContext().putStateFor(HausmeisterApp.GLOBAL_GEWAEHLTE_STOERUNG, stoerung);
            HausmeisterApp.wechsleSzene("aenderungMenue.fxml", "com.hausmeister.sample.aenderungMenue");
        }
    }

    /**
     * Die Methode öffnet ein Auswahlmenü, um ein jeweiliges Objekt aus der Liste zu erstellen.
     */
    public void anlegenMenue() {
        ChoiceDialog<String> dialog = new ChoiceDialog("STÖRUNG", "STÖRUNG", "STÖRUNG MIT DEADLINE", "RAUM", "STOCKWERK");
        dialog.setTitle("Auswal Anlegen");
        dialog.setHeaderText("Bitte Wählen Sie eine Option aus.");
        dialog.setContentText("Anlegen: ");
        Optional<String> result = dialog.showAndWait(); //liefert ein Optinal zurück
        if (result.isPresent()) { //wenn etwas zurück geliefert wurde
            try {
                if (result.get().equals("STÖRUNG")) {
                    neueStoerungAnlegen();
                } else if (result.get().equals("STÖRUNG MIT DEADLINE")) {
                    neueStoerungMitDeadlineAnlegen();
                } else if (result.get().equals("RAUM")) {
                    neuenRaumAnlegen();
                } else if (result.get().equals("STOCKWERK")) {
                    neuesStockwerkAnlegen();
                }
            } catch (NumberFormatException numberFormatException) { //unchecked Exception
                HausmeisterApp.zeigeFehlerDialog("Keine Auswahl getroffen.");
            }
        }
    }

    /**
     * Die Methode implementiert das Löschen von Störungen aus der Liste.
     */
    public void loeschenStoerung() {
        Stoerung stoerung = this.tableView.getSelectionModel().getSelectedItem(); //zuweisen des ausgewählten Objekts
        if (stoerung == null) {
            HausmeisterApp.zeigeFehlerDialog("Bitte wählen Sie eine Stoerung aus.");
        } else {
            try {
                getSammlungStoerungen().stoerungEntfernen(stoerung);
            } catch (InvalidStoerungException invalidStoerungException) {
                HausmeisterApp.zeigeFehlerDialog(invalidStoerungException.getMessage());
            }
        }
        refreshAllGuiValues();
    }

    /**
     * Die Methode implementiert das Löschen von Räumen aus der Liste.
     */
    public void loeschenRaum(){
        Raum raum = null;
        if (getRaumliste().getRaumListe().size() > 0) {
            raum = eingabeStoerungRaum();
        } else {
            HausmeisterApp.zeigeFehlerDialog("Raumliste hat keine Einträge.");
        }
        try {
            getRaumliste().entfernen(raum);
        } catch (InvalidRoomException invalidRoomException) {
            HausmeisterApp.zeigeFehlerDialog(invalidRoomException.getMessage());
        }

    }

    /**
     * Die Methode implementiert das Löschen von Stockwerken aus der Liste.
     */
    public void loeschenStockwerk(){
        Stockwerk stockwerk = null;
        if (getStockwerkListe().getStockwerkListe().size() > 0) {
            stockwerk = eingabeRaumStockwerk();
        } else {
            HausmeisterApp.zeigeFehlerDialog("Stockwerkliste hat keine Einträge.");
        }
        try {
            getStockwerkListe().entfernen(stockwerk);
        } catch (InvalidFloorException invalidFloorException) {
            HausmeisterApp.zeigeFehlerDialog(invalidFloorException.getMessage());
        }

    }

    /**
     * Die Methode implementiert das Anlegen einer neuen Störung und fügt sie der SammlungStoerung hinzu.
     */
    public void neueStoerungAnlegen() {

        if (getRaumliste().getRaumListe().size() > 0) {
            try {
                getSammlungStoerungen().stoerungHinzufuegen(new Stoerung(eingabeStoerungTitel(), eingabeStoerungBeschreibung(), eingabeStoerungGewerk(), eingabeStoerungRaum(), eingabeStoerungStatus()));
            } catch (InvalidInputException | InvalidStatusException | InvalidSubsectionsException | InvalidRoomException | InvalidStoerungException exception) {
                HausmeisterApp.zeigeFehlerDialog(exception.getMessage());
            }
        } else {
            HausmeisterApp.zeigeFehlerDialog("Kein Raum zum Anlegen einer Störung vorhanden.");
        }
        refreshAllGuiValues();
    }

    /**
     * Die Methode implementiert das Anlegen einer neuen Störung mit Deadline und fügt sie der SammlungStoerung hinzu.
     */
    public void neueStoerungMitDeadlineAnlegen() {
        if (getRaumliste().getRaumListe().size() > 0) {
            try {
                getSammlungStoerungen().stoerungHinzufuegen(new StoerungMitDeadline(eingabeStoerungTitel(), eingabeStoerungBeschreibung(), eingabeStoerungGewerk(), eingabeStoerungRaum(), eingabeStoerungStatus(), eingabeStoerungDeadlineDatum()));
            } catch (InvalidInputException | InvalidDateException | InvalidStatusException | InvalidSubsectionsException | InvalidRoomException | InvalidStoerungException exception) {
                HausmeisterApp.zeigeFehlerDialog(exception.getMessage());
            }
        } else {
            HausmeisterApp.zeigeFehlerDialog("Kein Raum zum Anlegen einer Störung vorhanden.");
        }
        refreshAllGuiValues();
    }

    /**
     * Die Methode implementiert das Anlegen eines neuen Raumes und fügt sie der Raumliste hinzu.
     */
    public void neuenRaumAnlegen() {
        if (getStockwerkListe().getStockwerkListe().size() > 0) {
            try {
                getRaumliste().hinzufuegen(new Raum(eingabeRaumNummer(), eingabeRaumBezeichnung(), eingabeRaumStockwerk()));
            } catch (InvalidRoomException | InvalidFloorException | InvalidInputException exception) {
                HausmeisterApp.zeigeFehlerDialog(exception.getMessage());
            }
        } else {
            HausmeisterApp.zeigeFehlerDialog("Kein Stockwerk zum Anlegen eines Raumes vorhanden");
        }

    }

    /**
     * Die Methode implementiert das Anlegen eines neuen Stockwerkes und fügt sie der Stockwerksliste hinzu.
     */
    public void neuesStockwerkAnlegen() {
        try {
            getStockwerkListe().hinzufuegen(new Stockwerk(eingabeStockwerkBezeichnung()));
        } catch (InvalidFloorException | InvalidInputException exception) {
            HausmeisterApp.zeigeFehlerDialog(exception.getMessage());
        }
    }


    /**
     * Die Methode fordert den Benutzer auf eine Raumnummer einzugeben und liefert die Eingabe als String zurück.
     *
     * @return Liefert eine Raumnummer vom Typ String zurück.
     */
    public String eingabeRaumNummer() {
        TextInputDialog dialog = new TextInputDialog("Raumnummer");
        dialog.setTitle("Eingabe Raumnummer");
        dialog.setHeaderText("Bitte geben Sie einene Raumnummer ein.");
        dialog.setContentText("Raumnummer: ");
        Optional<String> result = dialog.showAndWait(); //liefert ein Optinal zurück
        if (result.isPresent()) { //wenn etwas zurück geliefert wurde
            try {
                return result.get();
            } catch (NumberFormatException numberFormatException) { //unchecked Exception
                HausmeisterApp.zeigeFehlerDialog("Keine Raumnummer eingegeben.");
            }
        }
        return null;
    }

    /**
     * Die Methode fordert den Benutzer auf eine Raumbezeichnung einzugeben und liefert die Eingabe als String zurück.
     *
     * @return Liefert eine Raumbezeichnung vom Typ String zurück.
     */
    public String eingabeRaumBezeichnung() {
        TextInputDialog dialog = new TextInputDialog("Raumbezeichnung");
        dialog.setTitle("Eingabe Raumbezeichnung");
        dialog.setHeaderText("Bitte geben Sie einene Raumbezeichnung ein.");
        dialog.setContentText("Raumbezeichnung: ");
        Optional<String> result = dialog.showAndWait(); //liefert ein Optinal zurück
        if (result.isPresent()) { //wenn etwas zurück geliefert wurde
            try {
                return result.get();
            } catch (NumberFormatException numberFormatException) { //unchecked Exception
                HausmeisterApp.zeigeFehlerDialog("Keine Raumbezeichnung eingegeben.");
            }
        }
        return null;
    }

    /**
     * Die Methode fordert den Benutzer auf einen Stockwerk aus der Stockwerksliste auszuwählen, welches dann zurück
     * gegeben wird.
     *
     * @return Liefert ein Stockwerk vom Typ Stockwerk zurück.
     */
    public Stockwerk eingabeRaumStockwerk() {
        ChoiceDialog<Stockwerk> dialog = new ChoiceDialog(getStockwerkListe().getStockwerkListe().get(0), getStockwerkListe().getStockwerkListe());
        dialog.setTitle("Stockwerk wählen");
        dialog.setHeaderText("Bitte wählen Sie ein Stockwerk aus.");
        dialog.setContentText("Stockwerk: ");
        Optional<Stockwerk> stockwerk = dialog.showAndWait();
        if (stockwerk.isPresent()) { //wenn etwas zurück geliefert wurde
            try {
                return stockwerk.get();
            } catch (NumberFormatException numberFormatException) { //unchecked Exception
                HausmeisterApp.zeigeFehlerDialog("Kein Stockwerk gewählt.");
            }
        }
        return null;
    }

    /**
     * Die Methode fordert den Benutzer auf einen Stockwerksbezeichnung einzugebnen und liefert die Eingabe als
     * String zurück.
     *
     * @return Liefert eine Stockwerksbezeichnung vom Typ String zurück.
     */
    public String eingabeStockwerkBezeichnung() {
        TextInputDialog dialog = new TextInputDialog("Stockwerkbezeichnung");
        dialog.setTitle("Eingabe Stockwerkbezeichnung");
        dialog.setHeaderText("Bitte geben Sie einene Raumbezeichnung ein.");
        dialog.setContentText("Stockwerkbezeichnung: ");
        Optional<String> result = dialog.showAndWait(); //liefert ein Optinal zurück
        if (result.isPresent()) { //wenn etwas zurück geliefert wurde
            try {
                return result.get();
            } catch (NumberFormatException numberFormatException) { //unchecked Exception
                HausmeisterApp.zeigeFehlerDialog("Keine Stockwerkbezeichnung eingegeben.");
            }
        }
        return null;
    }
}


