package ui;

import com.hausmeister.sample.HausmeisterApp;
import domain.interference.Stoerung;
import domain.interference.StoerungMitDeadline;
import exceptions.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.GregorianCalendar;

public class AenderungMenueConroller extends InputStoerungController {

    @FXML
    private TableView<Stoerung> tableView;

    @FXML
    private Button btnBack;

    private Stoerung stoerung = (Stoerung) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_GEWAEHLTE_STOERUNG);

    /**
     * Die Methode wird zum Initialisieren der Grafischen Oberfläche verwendet.
     */
    public void initialize() {
        this.populateTable();
        this.refreshAllGuiValues();
        this.backToMainButton();
    }

    /**
     * Setzt die Funktion die Zurück Buttons der GUI, damit zurück in das Hauptfenster gewechselt werden kann.
     */
    private void backToMainButton() {
        btnBack.setOnAction((ActionEvent e) -> HausmeisterApp.wechsleSzene("main.fxml", "com.hausmeister.sample.main"));
    }

    /**
     * Die Methode implementiert das Aktualisieren der Datenfelder, welche angezeigt werden.
     */
    private void refreshAllGuiValues() {
        tableView.getItems().setAll((Stoerung) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_GEWAEHLTE_STOERUNG));
    }

    /**
     * DIe Methode implementiert das Erzeugen einer Tabelle mit definierten Spalten und fügt Datensätze aus der
     * Wallet hinzu.
     */
    private void populateTable() {
        //Spalten der Tabelle definieren über Getter-Methoden
        TableColumn<Stoerung, String> titel = new TableColumn<>("TITEL");
        titel.setCellValueFactory(new PropertyValueFactory<>("titel"));

        TableColumn<Stoerung, String> status = new TableColumn<>("STATUS");
        status.setCellValueFactory(new PropertyValueFactory<>("statusLabel"));

        TableColumn<Stoerung, String> gewerk = new TableColumn<>("GEWERK");
        gewerk.setCellValueFactory(new PropertyValueFactory<>("gewerkLabel"));

        TableColumn<Stoerung, String> beschreibung = new TableColumn<>("BESCHREIBUNG");
        beschreibung.setCellValueFactory(new PropertyValueFactory<>("beschreibung"));

        TableColumn<Stoerung, String> raumbezeichnung = new TableColumn<>("RAUM");
        raumbezeichnung.setCellValueFactory(new PropertyValueFactory<>("raum"));

        TableColumn<Stoerung, String> stockwerk = new TableColumn<>("STOCKWERK");
        stockwerk.setCellValueFactory(new PropertyValueFactory<>("stockwerk"));

        TableColumn<Stoerung, GregorianCalendar> erstelldatum = new TableColumn<>("ERSTELLDATUM");
        erstelldatum.setCellValueFactory(new PropertyValueFactory<>("erstellungsdatumToString"));

        TableColumn<Stoerung, GregorianCalendar> deadline = new TableColumn<>("DEADLINE");
        deadline.setCellValueFactory(new PropertyValueFactory<>("deadlineDatumToString"));

        //Spalten hinzufügen
        tableView.getColumns().clear(); //zu erst alles löschen
        tableView.getColumns().add(titel);
        tableView.getColumns().add(status);
        tableView.getColumns().add(gewerk);
        tableView.getColumns().add(beschreibung);
        tableView.getColumns().add(raumbezeichnung);
        tableView.getColumns().add(stockwerk);
        tableView.getColumns().add(erstelldatum);
        tableView.getColumns().add(deadline);
    }

    /**
     * Die Methode ermöglicht eine Änderung des Titels einer vorhandenen Störung.
     */
    public void aendernStoerungTitel() {
        try {
            getSammlungStoerungen().getSammlungStoerung().get(getSammlungStoerungen().getSammlungStoerung().indexOf(this.stoerung)).setTitel(eingabeStoerungTitel());
        } catch (InvalidInputException invalidInputException) {
            HausmeisterApp.zeigeFehlerDialog(invalidInputException.getMessage());
        }
        refreshAllGuiValues();
    }

    /**
     * Die Methode ermöglicht eine Änderung der Beschreibung einer vorhandenen Störung.
     */
    public void aendernStoerungBeschreibung() {
        try {
            getSammlungStoerungen().getSammlungStoerung().get(getSammlungStoerungen().getSammlungStoerung().indexOf(this.stoerung)).setBeschreibung(eingabeStoerungBeschreibung());
        } catch (InvalidInputException invalidInputException) {
            HausmeisterApp.zeigeFehlerDialog(invalidInputException.getMessage());
        }
        refreshAllGuiValues();
    }

    /**
     * Die Methode ermöglicht eine Änderung des Gewerkes einer vorhandenen Störung.
     */
    public void aendernStoerungGewerk() {
        try {
            getSammlungStoerungen().getSammlungStoerung().get(getSammlungStoerungen().getSammlungStoerung().indexOf(this.stoerung)).setGewaerk(eingabeStoerungGewerk());
        } catch (InvalidSubsectionsException invalidSubsectionsException) {
            HausmeisterApp.zeigeFehlerDialog(invalidSubsectionsException.getMessage());
        }
        refreshAllGuiValues();
    }

    /**
     * Die Methode ermöglicht eine Änderung des Raumes einer vorhandenen Störung.
     */
    public void aendernStoerungRaum() {
        try {
            getSammlungStoerungen().getSammlungStoerung().get(getSammlungStoerungen().getSammlungStoerung().indexOf(this.stoerung)).setRaum(eingabeStoerungRaum());
        } catch (InvalidRoomException invalidRoomException) {
            HausmeisterApp.zeigeFehlerDialog(invalidRoomException.getMessage());
        }
        refreshAllGuiValues();
    }

    /**
     * Die Methode ermöglicht eine Änderung des Status einer vorhandenen Störung.
     */
    public void aendernStoerungStatus() {
        try {
            getSammlungStoerungen().getSammlungStoerung().get(getSammlungStoerungen().getSammlungStoerung().indexOf(this.stoerung)).setStatus(eingabeStoerungStatus());
        } catch (InvalidStatusException invalidStatusException) {
            HausmeisterApp.zeigeFehlerDialog(invalidStatusException.getMessage());
        }
        refreshAllGuiValues();
    }

    /**
     * Die Methode ermöglicht eine Änderung der Deadline einer vorhandenen Störung.
     */
    public void aendernStoerungDeadline() {
        int index = getSammlungStoerungen().getSammlungStoerung().indexOf(this.stoerung);
        if (getSammlungStoerungen().getSammlungStoerung().get(index) instanceof StoerungMitDeadline) {
            popUpDatePicker();
            try {
                ((StoerungMitDeadline) getSammlungStoerungen().getSammlungStoerung().get(index)).setDeadline((GregorianCalendar) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_GEWAEHLTE_DEADLINE));
            } catch (InvalidDateException invalidDateException) {
                HausmeisterApp.zeigeFehlerDialog(invalidDateException.getMessage());
                invalidDateException.printStackTrace();
            }
        } else {
            HausmeisterApp.zeigeFehlerDialog("Es wurde keine Störung mit einer Deadline ausgewählt");
        }
        refreshAllGuiValues();
    }

}
