package com.hausmeister.sample;

import domain.DataStore;
import domain.buildingstructure.RaumListe;
import domain.buildingstructure.StockwerkListe;
import domain.interference.SammlungStoerungen;
import exceptions.RetrieveDataException;
import exceptions.SaveDataException;
import infrastruktur.FileDataStore;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import ui.GlobalContext;

import java.io.IOException;
import java.util.ResourceBundle;

public class HausmeisterApp extends Application {

    public static Stage mainStage;
    public static final String GLOBAL_SAMMLUNG_STOERUNGEN = "sammlungStoerungen";
    public static final String GLOBAL_RAUM_LISTE = "raumliste";
    public static final String GLOBAL_STOCKWERK_LISTE = "stockwerkliste";
    public static final String GLOBAL_GEWAEHLTE_STOERUNG = "gewaehlteStoerung";
    public static final String GLOBAL_GEWAEHLTE_DEADLINE = "gewaehlteDeadline";
    private static final DataStore DATA_STORE = new FileDataStore();

    /**
     * Die Methode dient zum Wechseln der Szenen.
     * @param fxmlFile übergebnes fxml-File
     * @param resourceBundle übergebenes reccource-bundle
     */
    public static void wechsleSzene(String fxmlFile, String resourceBundle) {
        try {
            Parent root = FXMLLoader.load(HausmeisterApp.class.getResource(fxmlFile), ResourceBundle.getBundle(resourceBundle));
            Scene scene = new Scene(root);
            mainStage.setScene(scene);
            mainStage.show();
        } catch (Exception exception) { //um jede Exception abzufangen
            HausmeisterApp.zeigeFehlerDialog("Could not load new scene!");
            exception.printStackTrace();
        }
    }

    /**
     * Zeigt die übergebene Nachricht in Form eines Alerts an.
     * @param message übergebene Nachricht vom Typ String.
     */
    public static void zeigeFehlerDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(message);
        alert.showAndWait();
    }

    /**
     * Die Methode wird beim Start ausgeführt und stellt die letzte Sitzung wieder her.
     * @param stage übergebene Stage
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        SammlungStoerungen sammlungStoerungen = new SammlungStoerungen();
        RaumListe raumliste = new RaumListe();
        StockwerkListe stockwerkListe = new StockwerkListe();

        mainStage = stage; //setzen der Stage beim Starten
        try {
            sammlungStoerungen = DATA_STORE.auslesenSammlungStoerung();
            System.out.println("Die SammlungStoerung wurde geladen.");
        } catch (RetrieveDataException retrieveDataException) {
            System.out.println("Fehler beim Laden der SammlungStoerung Datei. Es wird eine Leere Liste verwendet!");
            retrieveDataException.printStackTrace();
        }

        try {
            raumliste = DATA_STORE.auslesenRaumListe();
            System.out.println("Die Raumliste wurde geladen.");
        } catch (RetrieveDataException retrieveDataException) {
            System.out.println("Fehler beim Laden der Raumliste Datei. Es wird eine Leere Liste verwendet!");
            retrieveDataException.printStackTrace();
        }

        try {
            stockwerkListe = DATA_STORE.auslesenStockwerkListe();
            System.out.println("Die StockwerkListe wurde geladen.");
        } catch (RetrieveDataException retrieveDataException) {
            System.out.println("Fehler beim Laden der Stockwerkliste Datei. Es wird eine Leere Liste verwendet!");
            retrieveDataException.printStackTrace();
        }

        //In den GlobalContext speichern
        GlobalContext.getGlobalContext().putStateFor(HausmeisterApp.GLOBAL_SAMMLUNG_STOERUNGEN, sammlungStoerungen);
        GlobalContext.getGlobalContext().putStateFor(HausmeisterApp.GLOBAL_RAUM_LISTE, raumliste);
        GlobalContext.getGlobalContext().putStateFor(HausmeisterApp.GLOBAL_STOCKWERK_LISTE, stockwerkListe);

        //DummyDateien.dummyDateienGenerieren(sammlungStoerungen, stockwerkListe, raumliste);
        HausmeisterApp.wechsleSzene("main.fxml", "com.hausmeister.sample.main");

        //so kann das Programm nicht mehr durch das X geschlossen werden
        mainStage.setOnCloseRequest(event -> event.consume());
    }

    /**
     * Die Methode ermöglicht das Speichern der aktuellen Sitzung und Schreiben in eine Binärdatei.
     */
    @Override
    public void stop() {
        SammlungStoerungen sammlungStoerungen = (SammlungStoerungen) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_SAMMLUNG_STOERUNGEN);
        RaumListe raumListe = (RaumListe) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_RAUM_LISTE);
        StockwerkListe stockwerkListe = (StockwerkListe) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_STOCKWERK_LISTE);

        try {
            DATA_STORE.speichernSammlungStoerungen(sammlungStoerungen);
            System.out.println("Die SammlungStoerung wurde gespeichert.");
        } catch (SaveDataException saveDataException) {
            System.out.println("Die SammlungStoerung konnte nicht in die Datei geschrieben werden.");
            System.out.println(saveDataException.getMessage());
        }
        try {
            DATA_STORE.speichernRaumListe(raumListe);
            System.out.println("Die RaumListe wurde gespeichert.");
        } catch (SaveDataException saveDataException) {
            System.out.println("Die RaumListe konnte nicht in die Datei geschrieben werden.");
            System.out.println(saveDataException.getMessage());
        }
        try {
            DATA_STORE.speichernStockwerkliste(stockwerkListe);
            System.out.println("Die Stockwerkliste wurde gespeichert.");
        } catch (SaveDataException saveDataException) {
            System.out.println("Die StockwerksListe konnte nicht in die Datei geschrieben werden.");
            System.out.println(saveDataException.getMessage());
        }

    }


    public static void main(String[] args) {
        launch(args);
    }
}