package exceptions;
/**
 * @author mathiasrudig
 * @version 1.0
 */
public class InvalidDateException extends Exception {
    public InvalidDateException(String message) {
        super(message);
    }
    public InvalidDateException() {
        super("Ungueltiges Datum");
    }

}
