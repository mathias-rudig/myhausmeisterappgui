package exceptions;
/**
 * @author mathiasrudig
 * @version 1.0
 */
public class InvalidStoerungException extends Exception {
    public InvalidStoerungException(String message) {
        super(message);
    }
    public InvalidStoerungException() {
        super("Ungueltige Stoerung");
    }
}
