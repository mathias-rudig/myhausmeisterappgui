package exceptions;
/**
 * @author mathiasrudig
 * @version 1.0
 */
public class InvalidSubsectionsException extends Exception {
    public InvalidSubsectionsException(String message) {
        super(message);
    }
    public InvalidSubsectionsException() {
        super("Ungueltiges Gewerk");
    }
}
