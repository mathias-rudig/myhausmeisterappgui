package exceptions;
/**
 * @author mathiasrudig
 * @version 1.0
 */
public class InvalidRoomException extends Exception {
    public InvalidRoomException(String message) {
        super(message);
    }
    public InvalidRoomException() {
        super("Ungueltiger Raum");
    }
}
