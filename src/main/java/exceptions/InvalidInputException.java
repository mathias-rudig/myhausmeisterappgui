package exceptions;
/**
 * @author mathiasrudig
 * @version 1.0
 */
public class InvalidInputException extends Exception {
    public InvalidInputException(String message) {
        super(message);
    }
    public InvalidInputException(){
        super("Maximale Anzahl an Zeichen überschritten");
    }
}
