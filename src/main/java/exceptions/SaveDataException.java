package exceptions;
/**
 * @author mathiasrudig
 * @version 1.0
 */
public class SaveDataException extends Exception {
    public SaveDataException(String message) {
        super(message);
    }
    public SaveDataException(){
        super("Fehler beim Speichern der Datei");
    }
}